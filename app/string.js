//Задача 1

/* УСЛОВИЕ ЗАДАЧИ
Напишите функцию ucFirst(str), возвращающую строку str 
с заглавным первым символом. 
Например:
ucFirst("вася") == "Васян";
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 1');

let name = prompt('Введите имя', 'Artem');
console.log(name[0].toUpperCase() + name.substr(1));


//Задача 2

/* УСЛОВИЕ ЗАДАЧИ
Проверка на спам
Напишите функцию checkSpam(str), возвращающую true, если str содержит 'viagra' или 'XXX', а иначе false.
Функция должна быть нечувствительна к регистру:
checkSpam('buy ViAgRA now') == true
checkSpam('free xxxxx') == true
checkSpam("innocent rabbit") == false
*/
//РЕШЕНИЕ. 

console.log('ЗАДАЧА 2');

function checkSpam(lowString) {
    let firstString = lowString.toLowerCase();
    let firstValue = 'viagra';
    let secondValue = 'xxx';
    return firstString.includes(firstValue)
        || firstString.includes(secondValue);
}
console.log(checkSpam('buy ViAgRA now'));
console.log(checkSpam('free xxxxx'));
console.log(checkSpam('innocent rabbit'));

//Задача 3

/* УСЛОВИЕ ЗАДАЧИ
Создайте функцию truncate(str, maxlength), которая проверяет длину строки str и, 
если она превосходит maxlength, 
заменяет конец str на "…", так, чтобы её длина стала равна maxlength.
Результатом функции должна быть та же строка, если усечение не требуется, либо, 
если необходимо, усечённая строка.
Например:
truncate("Вот, что мне хотелось бы сказать на эту тему:", 20) = "Вот, что мне хотело…"
truncate("Всем привет!", 20) = "Всем привет!"
*/
//РЕШЕНИЕ 

console.log('ЗАДАЧА 3');

truncate = (secondString, maxLength) =>
    secondString.length > maxLength
        ? `${secondString.slice(0, maxLength - 1)}...`
        : secondString;
console.log(truncate('Never gonna give you up, never gonna let you dawn', 31));

//Задача 4

/* УСЛОВИЕ ЗАДАЧИ
Есть стоимость в виде строки "$120". То есть сначала идёт знак валюты, а затем – число.
Создайте функцию extractCurrencyValue(str), которая будет из такой строки выделять числовое значение и возвращать его.
Например:
alert( extractCurrencyValue('$120') === 120 ); // true
*/
//РЕШЕНИЕ 

console.log('ЗАДАЧА 4');

function extractCurrencyValue(thirdString) {
    return + thirdString.slice(1);
}  
console.log(extractCurrencyValue('$300'));