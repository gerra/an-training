//Задача 1

/* УСЛОВИЕ ЗАДАЧИ
Создайте скрипт, который запрашивает ввод двух чисел (используйте prompt) и после показывает их сумму.
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 1');

let x = 10;
let y = 10;
function sumXY(x, y) {
    return x + y
}
console.log(x + y);

//Задача 2

/* УСЛОВИЕ ЗАДАЧИ
Создайте функцию readNumber, которая будет запрашивать ввод числового значения до тех пор, пока посетитель его не введёт.
Функция должна возвращать числовое значение.
Также надо разрешить пользователю остановить процесс ввода, отправив пустую строку или нажав «Отмена». В этом случае функция должна вернуть null.
*/
//РЕШЕНИЕ


console.log('ЗАДАЧА 2');

function readNumber() {
    let number;
    number = prompt('Введите значение', '');
    while (isNaN(number) === true) {
        number = prompt('Введите значение', '');
    }
    if (!number) {
        return null;
    } else {
    (!isFinite(number));
        return +number;
    }
}
console.log(readNumber());

//Задача 3

/* УСЛОВИЕ ЗАДАЧИ
Случайное число от min до max
важность: 2
Встроенный метод Math.random() возвращает случайное число от 0 (включительно) до 1 (но не включая 1)
Напишите функцию random(min, max), которая генерирует случайное число с плавающей точкой от min до max (но не включая max).
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 3');


function random() {
    Math.min = 1;
    Math.max = 100;
    return Math.random() * (Math.max - Math.min) + Math.min;
}
console.log(random(Math.min, Math.max));

//Задача 4

/* УСЛОВИЕ ЗАДАЧИ
Случайное целое число от min до max
Напишите функцию randomInteger(min, max), которая генерирует случайное целое (integer) число от min до max (включительно).
Любое число из интервала min..max должно появляться с одинаковой вероятностью.
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 4');

Math.min = 2;
Math.max = 200;
function randomInteger() { 
    return Math.floor(Math.random() * (Math.max - Math.min + 1)) + Math.min;
}
console.log(randomInteger(Math.min, Math.max));