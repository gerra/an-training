//Задача 1

/* УСЛОВИЕ ЗАДАЧИ
Создание калькулятора при помощи конструктора
Создайте функцию-конструктор Calculator, который создаёт объекты с тремя методами:

read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта.
sum() возвращает сумму введённых свойств.
mul() возвращает произведение введённых свойств.
Например:

let calculator = new Calculator();
calculator.read();

alert( "Sum=" + calculator.sum() );
alert( "Mul=" + calculator.mul() );
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 1');

function Calculator(x, n) {
    this.x = 10,
    this.y = 20,
    this.read = (x, n) => { 
        this.mul = () => this.x * this.y;
        this.sum = () => this.x + this.y;
    };
};
let calculator = new Calculator();
calculator.read();
console.log(calculator.sum());
console.log(calculator.mul());

//Задача 2

/* УСЛОВИЕ ЗАДАЧИ
Напишите функцию-конструктор Accumulator(startingValue).
Объект, который она создаёт, должен уметь следующее:
Хранить «текущее значение» в свойстве value. Начальное значение устанавливается в аргументе конструктора startingValue.
Метод read() использует prompt для получения числа и прибавляет его к свойству value.
Таким образом, свойство value является текущей суммой всего, что ввёл пользователь при вызовах метода read(), с учётом начального значения startingValue.
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 2');

function Accumulator(startingValue) {
    this.value = startingValue;
    this.read = (x) => {
        this.value += 10;
    };
}
let accumulator = new Accumulator(1000);
accumulator.read();
console.log(accumulator.value);