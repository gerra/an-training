//Задача 1

/* УСЛОВИЕ ЗАДАЧИ
Проверка значения из диапазона
Напишите условие if для проверки, что переменная age находится в диапазоне между 14 и 90 включительно.
«Включительно» означает, что значение переменной age может быть равно 14 или 90.
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 1');

let age = prompt('Введите значение', '');
if (age >= 14 && age <= 90) {
    console.log('Переменная находится в диапазона');
} else {
    console.log('Переменная находится вне диапазона');
}

//Задача 2

/* УСЛОВИЕ ЗАДАЧИ
Проверка значения вне диапазона
Напишите условие if для проверки, что значение переменной age НЕ находится в диапазоне 14 и 90 включительно.
Напишите два варианта: первый с использованием оператора НЕ !, второй – без этого оператора.
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 2.1');

let age2 = prompt('Введите значение', '');
if (age2 < 14 || age2 > 90) {
    console.log('Переменная находится вне диапазона');
}

console.log('ЗАДАЧА 2.2');

let age3 = prompt('Введите значение', '');
if (!(age3 >= 14 && age3 <= 90)) {
    console.log('Переменная находится вне диапазона');
}

//Задача 3

/* УСЛОВИЕ ЗАДАЧИ
Напишите код, который будет спрашивать логин с помощью prompt.
Если посетитель вводит «Админ», то prompt запрашивает пароль, если ничего не введено или нажата клавиша Esc – показать «Отменено», в противном случае отобразить «Я вас не знаю».
Пароль проверять так:
    Если введён пароль «Я главный», то выводить «Здравствуйте!»,
    Иначе – «Неверный пароль»,
    При отмене – «Отменено».
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 3');

let login = prompt('Ты кто такой?', '');
if (login === 'Админ') {
    let pass = prompt('Введите пароль','');
        if (pass === 'Король Админов') {
            console.log('Ave Админ');
        }   
        else {
            pass === null || pass === ''
                ? console.log('Отменено')
                : console.log('Ты не Админ');
        }
}
else {
    login === null || login === ''
        ? console.log('Отменено')
        : console.log('Тебе тут не рады');
}