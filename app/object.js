//Задача 1//

/* УСЛОВИЕ ЗАДАЧИ

Привет, object
Напишите код, выполнив задание из каждого пункта отдельной строкой:

Создайте пустой объект user.
Добавьте свойство name со значением John.
Добавьте свойство surname со значением Smith.
Измените значение свойства name на Pete.
Удалите свойство name из объекта.

*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 1');

let user = {};
user.name = 'John';
user.secondName = 'Smith';
user.name = 'Pete';
delete user.name;

//Задача 2

/* УСЛОВИЕ ЗАДАЧИ

Проверка на пустоту
Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.
Должно работать так:
let schedule = {};
alert( isEmpty(schedule) ); // true
schedule["8:30"] = "get up";
alert( isEmpty(schedule) ); // false

*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 2');

let property = {};
function isEmpty(object) {
    for (let property in object) {
        return false;
    }
    return true;
}
console.log(isEmpty(property));

//Задача 3

/* УСЛОВИЕ ЗАДАЧИ

Проверка на пустоту
Сумма свойств объекта
У нас есть объект, в котором хранятся зарплаты нашей команды:
let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130
}
Напишите код для суммирования всех зарплат и сохраните результат в переменной sum. Должно получиться 390.
Если объект salaries пуст, то результат должен быть 0.
*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 3');

let pay = {
    John: 100,
    Ann: 160,
    Pete: 130
}
let sum = 0;
for (let key in pay) {
    sum += pay[key];
}
console.log(sum);

//Задача 4

/* УСЛОВИЕ ЗАДАЧИ /* 

Умножаем все числовые свойства на 2
Создайте функцию multiplyNumeric(obj), которая умножает все числовые свойства объекта obj на 2.
Например:
// до вызова функции
let menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

multiplyNumeric(menu);

// после вызова функции
menu = {
  width: 400,
  height: 600,
  title: "My menu"
};
Обратите внимание, что multiplyNumeric не нужно ничего возвращать. Следует напрямую изменять объект.
P.S. Используйте typeof для проверки, что значение свойства числовое.

*/
//РЕШЕНИЕ

console.log('ЗАДАЧА 4');
let menu = {
    width: 200,
    height: 300,
    title : 'My menu'
};
function multiplyNumeric(object) {
    for (let property in object) {
        if (typeof object[property] === 'number') {
            object[property] *= 2;
        }
    }
}
multiplyNumeric(menu);
console.log(menu);